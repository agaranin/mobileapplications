package com.example.artem_mid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    EditText editTextName;
    RadioGroup radioGroupMenu;
    ImageView imageView;
    Button buttonFinish, buttonOrder;
    public static final int ADDRESS_SEND_REQUEST = 1;
    TextView textViewAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        editTextName = findViewById(R.id.editTextName);
        textViewAddress = findViewById(R.id.textViewAddress);
        radioGroupMenu = findViewById(R.id.radioGroupMenu);
        imageView = findViewById(R.id.imageView);
        buttonFinish = findViewById(R.id.buttonFinish);
        buttonOrder = findViewById(R.id.buttonOrder);
        buttonOrder.setOnClickListener((View v) -> {
            showOrder();
        });
        buttonFinish.setOnClickListener((View v) -> {
            finish();
        });
        radioGroupMenu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonPoutine:
                        imageView.setImageResource(R.drawable.poutine);
                        break;
                    case R.id.radioButtonChefPoutine:
                        imageView.setImageResource(R.drawable.chef_poutine);
                        break;
                    case R.id.radioButtonSalmon:
                        imageView.setImageResource(R.drawable.salmon);
                        break;
                    case R.id.radioButtonSushi:
                        imageView.setImageResource(R.drawable.poutine);
                        break;
                    case R.id.radioButtonTacos:
                        imageView.setImageResource(R.drawable.tacos);
                        break;
                }
            }
        });
    }

    private void showOrder() {
        int selectedRadioBtn = radioGroupMenu.getCheckedRadioButtonId();
        String selectedFood = "";
        switch (selectedRadioBtn) {
            case R.id.radioButtonPoutine:
                selectedFood = "Poutine";
                imageView.setImageResource(R.drawable.poutine);
                break;
            case R.id.radioButtonChefPoutine:
                selectedFood = "Chef Poutine";
                imageView.setImageResource(R.drawable.chef_poutine);
                break;
            case R.id.radioButtonSalmon:
                selectedFood = "Salmon";
                imageView.setImageResource(R.drawable.salmon);
                break;
            case R.id.radioButtonSushi:
                selectedFood = "Sushi";
                imageView.setImageResource(R.drawable.poutine);
                break;
            case R.id.radioButtonTacos:
                selectedFood = "Tacos";
                imageView.setImageResource(R.drawable.tacos);
                break;
        }
        String userName = editTextName.getText().toString();
        Intent myIntent = new Intent(MainActivity.this, OrderActivity.class);
        myIntent.putExtra("userName", userName);
        myIntent.putExtra("selectedFood", selectedFood);
        startActivityForResult(myIntent, ADDRESS_SEND_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADDRESS_SEND_REQUEST) {
            String receivedData = (String) data.getStringExtra(OrderActivity.EXTRA_RETURN_MESSAGE);
            if (resultCode == RESULT_OK) {
                textViewAddress.setText("Address: " + receivedData);
            } else
                textViewAddress.setText("Address:");
        }
    }
}