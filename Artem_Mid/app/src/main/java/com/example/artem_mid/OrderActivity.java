package com.example.artem_mid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OrderActivity extends Activity {
    public static final String EXTRA_RETURN_MESSAGE = "extra_return_message";
    TextView textViewCustomerName, textViewCustomerOrder;
    Button buttonBack;
    EditText editTextAddress;
    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        initialize();
        getDataFromMain();
        sendAddress();
    }

    private void getDataFromMain() {
        userName = (String) getIntent().getStringExtra("userName");
        String selectedFood = (String) getIntent().getStringExtra("selectedFood");
        textViewCustomerName.setText(userName);
        textViewCustomerOrder.setText(selectedFood);
    }

    private void initialize() {
        buttonBack = findViewById(R.id.buttonBack);
        textViewCustomerName = findViewById(R.id.textViewCustomerName);
        textViewCustomerOrder = findViewById(R.id.textViewCustomerOrder);
        editTextAddress = findViewById(R.id.editTextAddress);
        userName = "Anonymous";
    }

    private void sendAddress() {
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strToToast = String.format("Thank you for using our application, %s", userName);
                Toast.makeText(OrderActivity.this, strToToast, Toast.LENGTH_LONG).show();

                String userAddress = editTextAddress.getText().toString();
                if (userAddress.isEmpty()) editTextAddress.setHint("Enter your address!");
                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_RETURN_MESSAGE, userAddress);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}