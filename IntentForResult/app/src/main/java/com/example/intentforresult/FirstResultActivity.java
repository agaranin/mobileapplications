package com.example.intentforresult;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.intentforresult.model.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FirstResultActivity extends AppCompatActivity {

    public static final String EXTRA_RETURN_MESSSAGE="";
    ListView listViewResult;
    Button buttonReturn;
    ArrayList<Person> personList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_result);
        getDataFromMain();
        sendResult();
    }

    private void sendResult() {
        buttonReturn = findViewById(R.id.buttonReturn);
        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_RETURN_MESSSAGE,"This is response");
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

/*    private void getDataFromMain() {
        Bundle bundle = getIntent().getBundleExtra("extraIntent");
        personList = (ArrayList<Person>) bundle.getSerializable("personBundle");
        listViewResult = findViewById(R.id.listViewResult);
        ArrayAdapter<Person> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, personList);
        listViewResult.setAdapter(arrayAdapter);
    }*/

    private void getDataFromMain() {
        personList = (ArrayList<Person>) getIntent().getSerializableExtra("extraIntent");
        listViewResult = findViewById(R.id.listViewResult);
        ArrayAdapter<Person> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, personList);
        listViewResult.setAdapter(arrayAdapter);
    }


}