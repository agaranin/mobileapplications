package com.example.intentforresult;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.intentforresult.model.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int ARRAY_SEND_REQUEST = 1;
    public static final int TEXT_SEND_REQUEST = 2;
    private int counter = 1;

    TextView textViewResult;
    Button buttonSend;
    List<Person> listPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeData();
        sendData();
    }

    private void sendData() {
        textViewResult = findViewById(R.id.textViewResult);
        buttonSend = findViewById(R.id.buttonSendObjects);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
/*            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("personBundle",(Serializable) listPerson);

                Intent myIntent = new Intent(MainActivity.this, FirstResultActivity.class);
                myIntent.putExtra("extraIntent",bundle);
                //startActivity(myIntent); // if we don't expect any result
                startActivityForResult(myIntent,ARRAY_SEND_REQUEST);
            }*/

            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, FirstResultActivity.class);
                myIntent.putExtra("extraIntent",(Serializable) listPerson);
                startActivityForResult(myIntent,ARRAY_SEND_REQUEST);
            }
        });
    }

    private void initializeData() {
        listPerson = new ArrayList<Person>();
        listPerson.add(new Person("ram", "montreal"));
        listPerson.add(new Person("harry", "toronto"));
        listPerson.add(new Person("tom", "vancouver"));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ARRAY_SEND_REQUEST) {

            String receivedData = (String) data.getStringExtra(FirstResultActivity.EXTRA_RETURN_MESSSAGE);
            if (resultCode == RESULT_OK)

                textViewResult.setText(receivedData);
            else
                textViewResult.setText("Canceled");
        }
    }

}