package com.example.listview_practice;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.listview_practice.model.Address;
import com.example.listview_practice.model.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    ListView personListView;
    List<Person> personList;
    ArrayAdapter<Person> arrayAdapter;

    static final int OBJECT_SEND_REQUEST=1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViewItems();
        initializeDummyPersonData();
    }

    private void initializeViewItems() {
        personListView = findViewById(R.id.personListView);
        personListView.setOnItemClickListener(this);
        personListView.setOnItemLongClickListener(this);
    }

    private void initializeDummyPersonData() {
        personList = new ArrayList<>();
        personList.add(new Person("Michael", new Address("Montreal", "Van Horne", "205")));
        personList.add(new Person("Ravi", new Address("Toronto", "Danforth street", "101")));
        personList.add(new Person("Andrey", new Address("Quebec", "Grande Alee", "312")));
        updateListView();
    }

    private void updateListView() {
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, personList);
        personListView.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("personBundle", (Serializable)parent.getItemAtPosition(position));
        bundle.putInt("selectedPosition", position);

        Intent myIntent = new Intent(this, PersonAddressEditActivity.class);
        myIntent.putExtra("extraIntent", bundle);

        startActivityForResult(myIntent, OBJECT_SEND_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OBJECT_SEND_REQUEST) {

            Person receivedPerson = (Person) data.getSerializableExtra(PersonAddressEditActivity.EXTRA_RETURN_OBJECT);
            int pos = (int) data.getIntExtra(PersonAddressEditActivity.EXTRA_RETURN_POS, 0);
            if (resultCode == RESULT_OK){
                //personList.get(pos).setAddress(receivedPerson.getAddress());
                personList.set(pos, receivedPerson);
                arrayAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Confirmation")
                .setMessage("Do you want to delete this person?")
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("Yes",
                        (dialogInterface, i) -> {
                        personList.remove(position);
                        arrayAdapter.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, "Person Deleted!", Toast.LENGTH_SHORT).show();
                })
                .setNegativeButton("No", null)
                .setNeutralButton("Cancel", null);
        builder.show();
        return true;
    }
}