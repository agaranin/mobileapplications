package com.example.listview_practice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.listview_practice.model.Address;
import com.example.listview_practice.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonAddressEditActivity extends AppCompatActivity {
    Person person;
    int pos;
    TextView textViewPersonNameData;
    EditText editTextCityData, editTextStreetData, editTextHouseNoData;
    Button buttonSave;
    static final String EXTRA_RETURN_OBJECT= "EXTRA_RETURN_OBJECT";
    static final String EXTRA_RETURN_POS= "EXTRA_RETURN_POS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_address_edit);
        getDataFromMain();
        sendResult();
    }

    private void sendResult() {
        buttonSave = findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = textViewPersonNameData.getText().toString();
                String city = editTextCityData.getText().toString();
                String street = editTextStreetData.getText().toString();
                String house = editTextHouseNoData.getText().toString();

                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_RETURN_POS, pos);
                returnIntent.putExtra(EXTRA_RETURN_OBJECT, new Person(name, new Address(city, street, house)));
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private void getDataFromMain() {
        Bundle bundle = getIntent().getBundleExtra("extraIntent");
        person = (Person)bundle.getSerializable("personBundle");
        pos =  bundle.getInt("selectedPosition");
        textViewPersonNameData = findViewById(R.id.textViewPersonNameData);
        editTextCityData = findViewById(R.id.editTextCityData);
        editTextStreetData = findViewById(R.id.editTextStreetData);
        editTextHouseNoData = findViewById(R.id.editTextHouseNoData);
        textViewPersonNameData.setText(person.getName());
        editTextCityData.setText(person.getAddress().getCity());
        editTextStreetData.setText(person.getAddress().getStreet());
        editTextHouseNoData.setText(person.getAddress().getHouse_no());
    }
}