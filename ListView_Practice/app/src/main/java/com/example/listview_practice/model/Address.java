package com.example.listview_practice.model;

import java.io.Serializable;

public class Address implements Serializable {
    private String city;
    private String street;
    private String house_no;

    public Address(String city, String street, String house_no) {
        this.city = city;
        this.street = street;
        this.house_no = house_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house_no='" + house_no + '\'' +
                '}';
    }
}
