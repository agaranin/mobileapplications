package com.example.mathquiz;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.mathquiz.models.Operation;
import com.example.mathquiz.models.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int USER_SEND_REQUEST = 1;
    public static final int btnDot = R.id.btnDot;
    public static final int btnMinus = R.id.btnMinus;
    public static final int btnClear = R.id.btnClear;
    public static final int btnGenerate = R.id.btnGenerate;
    public static final int btnValidate = R.id.btnValidate;
    public static final int btnScore = R.id.btnScore;
    public static final int btnFinish = R.id.btnFinish;
    EditText editTextAnswer;
    TextView textViewOperations, textViewStudentName, textViewStudentScore;
    User user;
    Operation currOperation;
    ArrayList<Operation> operations;
    double rightResult = 0;
    boolean isAnswerReceived = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        generateOperation();
        updateScore();
    }

    private void initialize() {
        editTextAnswer = findViewById(R.id.editTextAnswer);
        textViewOperations = findViewById(R.id.textViewOperations);
        textViewStudentName = findViewById(R.id.textViewStudentName);
        textViewStudentScore = findViewById(R.id.textViewStudentScore);
        // buttonIdArray declaration and initialization
        int[] buttonIdArray = {R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5,
                R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9, R.id.btnDot, R.id.btnMinus,
                R.id.btnClear, R.id.btnGenerate, R.id.btnValidate, R.id.btnScore, R.id.btnFinish};
        //buttonArray declaration
        Button[] buttonArray = new Button[buttonIdArray.length];
        // 0-9 buttons initialization and setting OnClickListener using for-loop
        for (int i = 0; i < buttonIdArray.length; i++) {
            buttonArray[i] = (Button) findViewById(buttonIdArray[i]);
            if(i<=9) {
                int buttonNumber = i; //make i effectively final variable to use it in a lambda expression
                buttonArray[i].setOnClickListener((View v) -> {
                    appendToAnswer(buttonNumber);
                });
            } else{
                buttonArray[i].setOnClickListener(this);
            }
        }
        // new User creation
        user = new User();
        user.setName("Anonymous");
        //operations ArrayList initialization
        operations = new ArrayList<>();
        user.setOperations(operations);
    }

    @Override
    public void onClick(View v) {
        switch (v. getId()) {
            case btnDot:
                appendToAnswer(".");
                break;
            case btnMinus:
                appendToAnswer("-");
                break;
            case btnClear:
                clearAnswer();
                break;
            case btnGenerate:
                generateOperation();
                break;
            case btnValidate:
                validate();
                break;
            case btnScore:
                showScore();
                break;
            case btnFinish:
                finish();
                break;
        }
    }

    private void appendToAnswer(int buttonNumber) {
        String buttonNumberStr = String.valueOf(buttonNumber);
        editTextAnswer.setText(editTextAnswer.getText().append(buttonNumberStr));
    }

    private void appendToAnswer(String buttonNumber) {
        editTextAnswer.setText(editTextAnswer.getText().append(buttonNumber));
    }

    private void clearAnswer() {
        editTextAnswer.setText(null);
    }

    private void showScore() {
        Intent myIntent = new Intent(MainActivity.this, Result.class);
        myIntent.putExtra("extraIntent",(Serializable) user);
        startActivityForResult(myIntent,USER_SEND_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USER_SEND_REQUEST) {
            String receivedData = (String) data.getStringExtra(Result.EXTRA_RETURN_MESSAGE);
            if (resultCode == RESULT_OK){
                user.setName(receivedData);
                textViewStudentName.setText(user.getName());
            }
            else
                textViewStudentName.setText(R.string.anonymous);
        }
    }

    private void validate() {
        if (editTextAnswer.getText().toString().isEmpty() || isAnswerReceived) return;
        double userAnswer = Double.parseDouble(editTextAnswer.getText().toString());
        String strResult;
        if(precise(userAnswer).equals(precise(rightResult))){
                strResult = getString(R.string.right_answer, precise(userAnswer));
            } else {
                strResult = getString(R.string.wrong_answer, precise(userAnswer), precise(rightResult));
            }
            textViewOperations.setText(textViewOperations.getText().toString().replace("?", strResult));
            currOperation.setAnswer(userAnswer);
            operations.add(currOperation);
            isAnswerReceived = true;
            updateScore();
            clearAnswer();
        }

        private String precise(double d){
            if(d % 1 != 0) {
                return String.format(Locale.US, "%.2f", d);
            } else{
                return String.format(Locale.US, "%.0f", d);
            }
        }

    private void updateScore(){
        textViewStudentScore.setText(String.format(Locale.US, "%d%%",user.getScore()));
    }

    private void generateOperation() {
        clearAnswer();
        isAnswerReceived = false;
        Random random = new Random();
        double operand1 = (double)random.nextInt(10);
        double operand2 = (double)random.nextInt(10);
        int operatorId = random.nextInt(4);
        String operator = "";
        switch (operatorId){
            case 0:
                operator = "+";
                rightResult = operand1 + operand2;
                break;
            case 1:
                operator = "-";
                rightResult = operand1 - operand2;
                break;
            case 2:
                operator = "*";
                rightResult = operand1 * operand2;
                break;
            case 3:
                operator = "/";
                while (operand2 == 0){
                    operand2 = random.nextInt(10);
                }
                rightResult = operand1 / operand2;
                break;
            default:
                operator = "+";
                rightResult = operand1 * operand2;
                break;
        }
        String operationStr = getString(R.string.math_operation, operand1, operator, operand2);
        textViewOperations.setText(operationStr);
        currOperation = new Operation();
        currOperation.setOperand1(operand1);
        currOperation.setOperand2(operand2);
        currOperation.setOperator(operatorId);
    }
}