package com.example.mathquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.mathquiz.models.Operation;
import com.example.mathquiz.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Result extends AppCompatActivity {
    public static final String EXTRA_RETURN_MESSAGE = "extra_return_message";
    public static final int radioButtonAll = R.id.radioButtonAll;
    public static final int radioButtonRight = R.id.radioButtonRight;
    public static final int radioButtonWrong = R.id.radioButtonWrong;
    public static final int radioButtonSortA = R.id.radioButtonSortA;
    public static final int radioButtonSortD = R.id.radioButtonSortD;
    ListView listViewResult;
    EditText editTextName;
    TextView textViewScore;
    Button btnShow, btnBack;
    ArrayList<Operation> operationList;
    ArrayList<Operation> OriginalOperationList;
    RadioGroup radioGroupSort;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initialize();
        getDataFromMain();
        sendResult();
        showResult();
    }

    private void showResult() {
        btnShow.setOnClickListener((View v) -> {
            int selectedRadioButton = radioGroupSort.getCheckedRadioButtonId();
            switch (selectedRadioButton) {
                case radioButtonAll:
                    bindToListView(OriginalOperationList);
                    break;
                case radioButtonRight:
                    ArrayList<Operation> rightOperationList = getRightOperationList(operationList);
                    bindToListView(rightOperationList);
                    break;
                case radioButtonWrong:
                    ArrayList<Operation> wrongOperationList = getWrongOperationList(operationList);
                    bindToListView(wrongOperationList);
                    break;
                case radioButtonSortA:
                    Collections.sort(operationList);
                    bindToListView(operationList);
                    break;
                case radioButtonSortD:
                    Collections.sort(operationList, Collections.reverseOrder());
                    bindToListView(operationList);
                    break;
            }
        });
    }

    private ArrayList<Operation> getRightOperationList(ArrayList<Operation> operationList) {
        ArrayList<Operation> rightOperationList = new ArrayList<>();
        for (Operation operation : operationList) {
            if (operation.isAnswerRight()) {
                rightOperationList.add(operation);
            }
        }
        return rightOperationList;
    }

    private ArrayList<Operation> getWrongOperationList(ArrayList<Operation> operationList) {
        ArrayList<Operation> wrongOperationList = new ArrayList<>();
        for (Operation operation : operationList) {
            if (!operation.isAnswerRight()) {
                wrongOperationList.add(operation);
            }
        }
        return wrongOperationList;
    }

    private void bindToListView(ArrayList<Operation> operationList) {
        ArrayAdapter<Operation> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, operationList);
        listViewResult.setAdapter(arrayAdapter);
    }

    private void initialize() {
        editTextName = findViewById(R.id.editTextName);
        textViewScore = findViewById(R.id.textViewScore);
        listViewResult = findViewById(R.id.listViewResult);
        radioGroupSort = findViewById(R.id.radioGroupSort);
        btnBack = findViewById(R.id.btnBack);
        btnShow = findViewById(R.id.btnShow);
    }

    private void getDataFromMain() {
        user = (User) getIntent().getSerializableExtra("extraIntent");
        textViewScore.setText(String.format(Locale.US, "%d%%", user.getScore()));
        operationList = user.getOperations();
        OriginalOperationList = ( ArrayList<Operation>)operationList.clone();
    }

    private void sendResult() {
        btnBack.setOnClickListener((View v) -> {
            String userName = editTextName.getText().toString();
            if (userName.isEmpty()) {
                editTextName.setHint(R.string.enter_your_name);
                return;
            }
            Intent returnIntent = new Intent();
            returnIntent.putExtra(EXTRA_RETURN_MESSAGE, userName);
            setResult(RESULT_OK, returnIntent);
            finish();
        });
    }
}