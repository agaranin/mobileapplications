package com.example.mathquiz.models;

import java.io.Serializable;
import java.util.Locale;

public class Operation implements Serializable, Comparable<Operation> {

    private double operand1;
    private double operand2;
    private int operator;
    private double answer;

    public double getOperand1() {
        return operand1;
    }

    public void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public double getResult() {
        switch (operator) {
            case 1:
                return operand1 - operand2;
            case 2:
                return operand1 * operand2;
            case 3:
                return operand1 / operand2;
            default:
                return operand1 + operand2;
        }
    }

    public double getAnswer() {
        return answer;
    }

    public void setAnswer(double answer) {
        this.answer = answer;
        isAnswerRight();
    }

    public Boolean isAnswerRight() {
        String stringUserAnswer = String.format(Locale.US, "%.2f", answer);
        String stringRightResult = String.format(Locale.US, "%.2f", getResult());
        return stringUserAnswer.equals(stringRightResult);
    }

    private String getOperatorStr() {
        switch (operator) {
            case 1:
                return "-";
            case 2:
                return "*";
            case 3:
                return "/";
            default:
                return "+";
        }
    }

    private String precise(double d) {
        if (d % 1 != 0) {
            return String.format(Locale.US, "%.2f", d);
        } else {
            return String.format(Locale.US, "%.0f", d);
        }
    }

    @Override
    public String toString() {

        return String.format(Locale.US, "%.0f %s %.0f = %s\nYour answer is %s\nRight answer is %s",
                operand1, getOperatorStr(), operand2, precise(answer), isAnswerRight() ? "right" : "wrong", precise(getResult()));
    }

    @Override
    public int compareTo(Operation o) {
        return isAnswerRight().compareTo(o.isAnswerRight());
    }
}
