package com.example.mathquiz.models;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    private String name;
    private ArrayList<Operation> operations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Operation> getOperations() {
        return operations;
    }

    public void setOperations(ArrayList<Operation> operations) {
        this.operations = operations;
    }

    public int getScore(){
        double numberOfRightAnswers = 0;
        for (Operation o: operations) {
            if(o.isAnswerRight())numberOfRightAnswers++;
        }
        return (int)((numberOfRightAnswers / operations.size()) * 100);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", operations=" + operations +
                '}';
    }
}
