package com.example.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editTextName;
    RadioGroup radioGroupSport, radioGroupSex;
    RadioButton radioMale, radioFemale, radioSoccer, radioHockey, radioHandball;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize(){
        editTextName = findViewById(R.id.editTextName);
        radioGroupSport = findViewById(R.id.radioGroupSport);
        radioGroupSex = findViewById(R.id.radioGroupSex);

    }
    public void showInfo(View view) {
        String name = editTextName.getText().toString();
        int selectedSexRadioBtn = radioGroupSex.getCheckedRadioButtonId();
        int selectedSportRadioBtn = radioGroupSport.getCheckedRadioButtonId();

        String strSex;
        if(selectedSexRadioBtn == R.id.radioFemale)
            strSex = "Female";
        else
            strSex = "Male";

        String strSport = null;
        switch (selectedSportRadioBtn){
            case R.id.radioSoccer:
                strSport = "Soccer";
                break;
            case R.id.radioHockey:
                strSport = "Hockey";
                break;
            case R.id.radioHandball:
                strSport = "Handball";
                break;
        }

        String strToToast = String.format("My name is %s\n I'm: %s\n Ma favorite sport is: %s",name, strSex, strSport);
        Toast.makeText(this,strToToast, Toast.LENGTH_LONG).show();
    }
}