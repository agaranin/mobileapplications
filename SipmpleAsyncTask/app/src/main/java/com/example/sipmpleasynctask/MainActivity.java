package com.example.sipmpleasynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText inputTime;
    Button buttonRunAsync;
    TextView finalResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        inputTime = findViewById(R.id.editTextSleepTime);
        buttonRunAsync = findViewById(R.id.buttonRunAsync);
        finalResult = findViewById(R.id.textViewResult);
        buttonRunAsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sleepTime = inputTime.getText().toString();
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(sleepTime);

            }
        });
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            String startText="Sleeping process started";
            finalResult.setText(startText);
        }
        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping for "+params[0]+" secs");// Calls onProgressUpdate()
            String resp;
            try {
                int time = Integer.parseInt(params[0]) * 1000; //converting to milliseconds
                Thread.sleep(time);
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            resp = "Slept for " + params[0] + " seconds";
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            finalResult.setText(result);
        }

        @Override
        protected void onProgressUpdate(String... text) {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    text[0]);
        }
    }
}