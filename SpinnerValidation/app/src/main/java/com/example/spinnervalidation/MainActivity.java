package com.example.spinnervalidation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    Spinner spinnerCalc;
    String selectedSpinnerText;
    int result, userAnswer;
    TextView textViewQuestion;
    EditText editTextAnswer;
    Button btnValidate, btnGenerate;
    String[] listOperation = new String[]{"Addition", "Subtraction", "Multiplication", "Division"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        Toast.makeText(this, "I selected ", Toast.LENGTH_LONG).show();
    }
    private void initialize() {
        spinnerCalc=findViewById(R.id.spinnerCalc);
        spinnerCalc.setOnItemSelectedListener(this);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOperation);
        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerCalc.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedSpinnerText= listOperation[position];
        Toast.makeText(this, "I selected "+selectedSpinnerText, Toast.LENGTH_LONG).show();
    }

    /*  @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    //        TextView spinnerText=(TextView) view;
    //        selectedSpinnerText=spinnerText.getText().toString();
            selectedSpinnerText= listOperation[position];
            Toast.makeText(this, "I selected "+selectedSpinnerText, Toast.LENGTH_LONG).show();
        }*/
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    @Override
    public void onClick(View v) {

    }
}