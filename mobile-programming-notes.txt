Programming code should be readable and maintainable
Maintainable - the problem of the RelativeLayout
Is a - правило для наименование классов наследования. (Teacher is a Person).
==============
An interface is a completely "abstract class" that is used to group related methods with empty bodies.
Notes on Interfaces:
Like abstract classes, interfaces cannot be used to create objects (in the example above, it is not possible to create an "Animal" object in the MyMainClass)
Interface methods do not have a body - the body is provided by the "implement" class
On implementation of an interface, you must override all of its methods
Interface methods are by default abstract and public
Interface attributes are by default public, static and final
An interface cannot contain a constructor (as it cannot be used to create objects)
Why And When To Use Interfaces?
1) To achieve security - hide certain details and only show the important details of an object (interface).

2) Java does not support "multiple inheritance" (a class can only inherit from one superclass). However, it can be achieved with interfaces, because the class can implement multiple interfaces. Note: To implement multiple interfaces, separate them with a comma (see example below).

Interface definition? (Interface talk abaut "What to do", Not "How to do").
===========================================================
Difference between Abstract Class and Interface in Java
Type of methods: Interface can have only abstract methods. An abstract class can have abstract and non-abstract methods. From Java 8, it can have default and static methods also.
Final Variables: Variables declared in a Java interface are by default final. An abstract class may contain non-final variables.
Type of variables: Abstract class can have final, non-final, static and non-static variables. The interface has only static and final variables.
Implementation: Abstract class can provide the implementation of the interface. Interface can’t provide the implementation of an abstract class.
Inheritance vs Abstraction: A Java interface can be implemented using the keyword “implements” and an abstract class can be extended using the keyword “extends”.
Multiple implementations: An interface can extend another Java interface only, an abstract class can extend another Java class and implement multiple Java interfaces.
Accessibility of Data Members: Members of a Java interface are public by default. A Java abstract class can have class members like private, protected, etc. 


========================================
Polymorphism - When you assign subtype class to a reference variable you willl have a different answer to a method from superclass

Polymorphism is the ability of an object to take on many forms. The most common use of polymorphism in OOP occurs when a parent class reference is used to refer to a child class object. Any Java object that can pass more than one IS-A test is considered to be polymorphic. ... A reference variable can be of only one type

The word polymorphism means having many forms. ... Real life example of polymorphism: A person at the same time can have different characteristic. Like a man at the same time is a father, a husband, an employee. So the same person posses different behavior in different situations. This is called polymorphism.
=======================================
Java 8 Defaults
Возможность импелементировать общий метод в интерфейсе. (Default implementation)
===================================================
 Serialization is the process of serializing the state of an object is represented and stored in the form of a sequence of bytes.

Serialization is the process of converting an object into a stream of bytes to store the object or transmit it to memory, a database, or a file. Its main purpose is to save the state of an object in order to be able to recreate it when needed. The reverse process is called deserialization.
======================================
A process is a program in execution.
A process is a running instance of a programm.
=========================
Thread - parallel execution path through a program. 
============================
Difference betweeen a process and a thread: One process can have one or more treads. It depends how you design it, how you manage it.
=============================
A heap is a tree base data structure. Tree is subset of graph. Heap is a complete binary tree. We have min-heap and max-heap. In min-heap each node is smaller than the children. In max heap each node is larger then the children.
================================================
Call Stack (last in - first out). For each thread you are going to have one stack.
We have two data structure. One of them is heap. When you create an object, it goes to the heap.

When a new thread is launched, the Java virtual machine creates a new Java stack for the thread. As mentioned earlier, a Java stack stores a thread's state in discrete frames. The Java virtual machine only performs two operations directly on Java Stacks: it pushes and pops frames.

a call stack( also known as stack) is a stack data structure that stores information about the active subroutines of a computer program.
=======================================
Generics is the capability to parameterize types. The idea is to allow type (Integer, String, … etc, and user-defined types) to be a parameter to methods, classes, and interfaces. Using Generics, it is possible to create classes that work with different data types.
===========================================
Android AsyncTask is an abstract class provided by Android which gives us the liberty to perform heavy tasks in the background and keep the UI thread light thus making the application more responsive.

AsyncTask is a mechanism for executing operations in a background thread without having to manually handle thread creation or execution.
============================
An event listener is an interface in the View class that contains a single callback method. These methods will be called by the Android framework when the View to which the listener has been registered is triggered by user interaction with the item in the UI.

